from django import forms

from .models import *
                                                                #problemas con los private fields, por lo que se opto por dejar uno solo por model
class Propietarioss(forms.ModelForm):
    class Meta:
        model = Propietario
        fields = ['nombre']

class Equiposs(forms.ModelForm):
    class Meta:
        model = Equipo
        fields = ['sede','nombre','propietario']

class Estadioss(forms.ModelForm):
    class Meta:
        model = Estadio
        fields = ['nombre','capacidad','tamano','propietario']
       
class Jugadoress(forms.ModelForm):
    class Meta:
        model = Jugador
        fields = ['equipo', 'nombre', 'numero', 'posicion']

