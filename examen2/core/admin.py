from django.contrib import admin

from .models import *

                                                                    #se importaron para un mejor manejo en el admin
@admin.register(Propietario)
class PropietarioAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "nombre",
    ]

@admin.register(Estadio)
class EstadioAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "nombre",
        "capacidad",
        "tamano",
        "propietario",
    ]

@admin.register(Equipo)
class EquipoAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "nombre",
        "sede",
        "propietario",
    ]

@admin.register(Jugador)
class JugadorAdmin(admin.ModelAdmin):
    list_display = [
        "equipo",
        "nombre",
        "numero",
        "posicion",
    ]
