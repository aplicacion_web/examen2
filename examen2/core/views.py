from django.views.generic import ListView               #se importo por las views 7 y 8
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic

from .forms import *
from .models import *

#Estadio

class Estadios(generic.View):
    template_name = "core/estadios.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Estadio.objects.all()
        self.context = {
            "estadios": queryset  
        }
        return render(request, self.template_name, self.context)



class DetailEstadio(generic.DetailView):
    template_name = "core/detail1.html"
    model = Estadio


class CreateEstadio(generic.CreateView):          #No se puede cambiar nombre del forms propietario por falta de widgets, por los private fields
    template_name = "core/create1.html"
    model = Estadio
    form_class = Estadioss
    success_url = reverse_lazy("core:estadios")


class UpdateEstadio(generic.UpdateView):
    template_name = "core/Update1.html"
    model = Estadio
    form_class = Estadioss
    success_url = reverse_lazy("core:estadios")

class DeleteEstadio(generic.DeleteView):
    template_name = "core/delete1.html"
    model = Estadio
    success_url = reverse_lazy("core:estadios")


#Equipos

class Equipos(generic.View):
    template_name = "core/equipos.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Equipo.objects.all()
        self.context = {
            "equipos": queryset
        }
        return render(request, self.template_name,self.context)
    
class CreateEquipo(generic.CreateView):
    template_name = "core/create2.html"
    model = Equipo
    form_class = Equiposs
    success_url = reverse_lazy("core:equipos")

    def form_valid(self, form):
        
        return super().form_valid(form)


class DetailEquipo(generic.DetailView):
    template_name = "core/detail2.html"
    model = Equipo

class UpdateEquipo(generic.UpdateView):
    template_name = "core/Update2.html"
    model = Equipo
    form_class = Equiposs
    success_url = reverse_lazy("core:equipos")

class DeleteEquipo(generic.DeleteView):
    template_name = "core/delete2.html"
    model = Equipo
    success_url = reverse_lazy("core:equipos")

#Jugadores

class Jugadores(generic.View):
    template_name = "core/jugadores.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Jugador.objects.all()
        self.context = {
            "jugador": queryset
        }
        return render(request, self.template_name,self.context)

class CreateJugador(generic.CreateView):
    template_name = "core/create3.html"
    model = Jugador
    form_class = Jugadoress
    success_url = reverse_lazy("core:jugadores")

class DetailJugador(generic.DetailView):
    template_name = "core/detail3.html"
    model = Jugador

class UpdateJugador(generic.UpdateView):
    template_name = "core/Update3.html"
    model = Jugador
    form_class = Jugadoress
    success_url = reverse_lazy("core:jugadores")

class DeleteJugador(generic.DeleteView):
    template_name = "core/delete3.html"
    model = Jugador
    success_url = reverse_lazy("core:jugadores")
    

#Propietarios

class Propietarios(generic.View):
    template_name = "core/propietarios.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Propietario.objects.all()
        self.context = {
            "propietario": queryset
        }
        return render(request, self.template_name,self.context)

class CreatePropietario(generic.CreateView):
    template_name = "core/create4.html"
    model = Propietario
    form_class = Propietarioss
    success_url = reverse_lazy("core:propietarios")

class UpdatePropietario(generic.UpdateView):
    template_name = "core/update4.html"
    model = Propietario
    form_class = Propietarioss
    success_url = reverse_lazy("core:propietarios")


class DeletePropietario(generic.DeleteView):
    template_name = "core/delete4.html"
    model = Propietario
    success_url = reverse_lazy("core:propietarios")


#jugadores-equipo


class EquiposJugadores(generic.View):
    template_name = "core/jugadoresequipo.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Equipo.objects.all()
        self.context = {
            "equipos": queryset
        }
        return render(request, self.template_name,self.context)

class Jugadores7(ListView):
    model = Jugador
    template_name = 'core/jugadores7.html'
    context_object_name = 'jugadores'

    def get_queryset(self):                 #se obtiene el id de equipo para filtrar
        equipo_id = self.kwargs['pk']  
        return Jugador.objects.filter(equipo_id=equipo_id)

    def get_context_data(self, **kwargs):                #qui es con la url
        context = super().get_context_data(**kwargs)
        equipo_id = self.kwargs['pk']  
        context['equipo_id'] = equipo_id
        return context

class Jugadores8(ListView):
    model = Jugador
    template_name = 'core/jugadores8.html'
    context_object_name = 'jugadores'
                                                    #se realiza lo mismo que con el jugadores7
    def get_queryset(self):
        equipo_id = self.kwargs['pk']  
        return Jugador.objects.filter(equipo_id=equipo_id)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        equipo_id = self.kwargs['pk'] 
        context['equipo_id'] = equipo_id
        return context

