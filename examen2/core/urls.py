from core import views
from django.urls import path

from .models import *

app_name = "core"
urlpatterns = [
    
    #Propietario
    path('lista/propietario/',views.Propietarios.as_view(), name="propietarios"),
    path('create/propietario/',views.CreatePropietario.as_view(), name="create4"),
    path('update/propietario/<int:pk>/', views.UpdatePropietario.as_view(), name= "update4"),
    path('delete/propietario/<int:pk>/', views.DeletePropietario.as_view(), name= "delete4"),
    
    #Estadio
    path('lista/estadio/',views.Estadios.as_view(), name="estadios"),                               #numeros para mayor facilidad, ademas se acomodo tomando en cuenta los models
    path('create/estadio/',views.CreateEstadio.as_view(), name="create1"),
    path('detail/estadio/<int:pk>/', views.DetailEstadio.as_view(), name= "detail1"),
    path('update/estadio/<int:pk>/', views.UpdateEstadio.as_view(), name= "Update1"),
    path('delete/estadio/<int:pk>/', views.DeleteEstadio.as_view(), name= "delete1"),
    #Equipo
    path('lista/equipo/',views.Equipos.as_view(), name="equipos"),
    path('create/equipo/',views.CreateEquipo.as_view(), name="create2"),
    path('detail/equipo/<int:pk>/', views.DetailEquipo.as_view(), name= "detail2"),
    path('update/equipo/<int:pk>/', views.UpdateEquipo.as_view(), name= "Update2"),
    path('delete/equipo/<int:pk>/', views.DeleteEquipo.as_view(), name= "delete2"),
    #Jugador
    path('lista/jugador/',views.Jugadores.as_view(), name="jugadores"),
    path('create/jugador/',views.CreateJugador.as_view(), name="create3"),
    path('detail/jugador/<int:pk>/', views.DetailJugador.as_view(), name= "detail3"),
    path('update/jugador/<int:pk>/', views.UpdateJugador.as_view(), name= "Update3"),
    path('delete/jugador/<int:pk>/', views.DeleteJugador.as_view(), name= "delete3"),
    
    #Jugadores-equipos
    path('lista/equipos/jugadores/',views.EquiposJugadores.as_view(), name="jugadoresequipo"),
    path('equipos/<int:pk>/', views.Jugadores7.as_view(), name='jugadores7'),
    path('equipos/posicion/<int:pk>/', views.Jugadores8.as_view(), name='jugadores8'),
]