from django.db import models
from django.core.validators import MinValueValidator

class Propietario(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'Propietario'

class Equipo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, null=True, blank=True)
    sede = models.CharField(max_length=100, null=True, blank=True)
    propietario = models.ForeignKey(Propietario, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'Equipo'

class Jugador(models.Model):
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100, null=True, blank=True)
    numero = models.IntegerField(primary_key=True)
    posicion = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        managed = True
        db_table = 'Jugador'

class Estadio(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    capacidad = models.IntegerField(validators=[MinValueValidator(1)], null=True, blank=True)
    tamano = models.IntegerField(validators=[MinValueValidator(1)],null=True, blank=True)
    propietario = models.ForeignKey(Propietario, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'Estadio'

